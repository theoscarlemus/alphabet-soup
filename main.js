const fs = require('fs'); 
const { wordSearch } = require('./search');

//Read the contents of the file 
function readFile(file){
    try{
        let lines = []; 
        const fileContents = fs.readFileSync(file, 'utf-8');
        fileContents.split(/\r?\n/).forEach(line => {
            lines.push(line) 
        })
        return lines; 
    }catch{
        console.error("Error with file")
        return false; 
    }
}

//create the matrix 
function createMatrix(data){
    let matrix = []; 
    let temp=[]; 
    for(let i = 0; i < data.length; i++){
        temp = data[i].split(' '); 
        matrix.push(temp); 
    }
    return matrix;  
}

const myArgs = process.argv.slice(2);
if(myArgs.length > 1){
    throw new Error('Only one file at a time please!')
}

//get data from file
let file = readFile(myArgs[0])

//if file is wrong throw errow
if(!file){
    throw new Error('Please enter a valid file')
}

//get columns and rows from file 
let dimensions = file[0].split('x'); 

//getData to fill matrix
let data = [];  
for(let i = 0; i < dimensions[1]; i++)
{
    data.push(file[i+1])
}

//get the words to search 
let words = []; 
let i = parseInt(dimensions[1]) + 1; 

for(i; i < file.length; i++){
    words.push(file[i]); 
}

//create the matrix 
let matrix = createMatrix(data);

//check if word is in matrix
for(let w = 0; w<words.length; w++){
    wordSearch(matrix, words[w], dimensions[0], dimensions[1]); 
}



//console.log(matrix); 
//console.log(file[0]); 
