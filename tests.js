const { wordSearch } = require('./search');
const assert = require('assert')

//test found 
//test small 10x10 matrix 
function testSmallFound(){
    let matrix = [];
    let word = "strawberry"
    let temp=[]; 
    for(let i = 0; i < 10; i++){
        temp = word.split(''); 
        matrix.push(temp); 
    }
    //console.log(matrix)
    return wordSearch(matrix, 'be', 10, 10); 
}
//test big 100x100
function testLargeFound(){
    let matrix = [];
    let string = "qazxswedcrfvbgtyhnmjuiklopoiuytrewsdcfdtyhgfdewasfgtyuikjhgrtrertyuiopiuhgfdsertyuijhgfdsertyujnmbvf"; 
    let word = "sert"
    let temp=[]; 
    for(let i = 0; i < 100; i++){
        temp = string.split(''); 
        matrix.push(temp); 
    }
    //console.log(matrix)
    return wordSearch(matrix, word, 100, 100); 
}

//test not found 
function testSmallNotFound(){
    let matrix = [];
    let string = "strawberry"; 
    let word = "pool"; 
    let temp=[]; 
    for(let i = 0; i < 10; i++){
        temp = string.split(''); 
        matrix.push(temp); 
    }
    //console.log(matrix)
    return wordSearch(matrix, word, 10, 10); 
}
//test big
//test big 100x100
function testLargeFound(){
    let matrix = [];
    let string = "qazxswedcrfvbgtyhnmjuiklopoiuytrewsdcfdtyhgfdewasfgtyuikjhgrtrertyuiopiuhgfdsertyuijhgfdsertyujnmbvf"; 
    let word = "dewas"
    let temp=[]; 
    for(let i = 0; i < 100; i++){
        temp = string.split(''); 
        matrix.push(temp); 
    }
    //console.log(matrix)
    return wordSearch(matrix, word, 100, 100); 
}

function testLargeNotFound(){
    let matrix = [];
    let string = "qazxswedcrfvbgtyhnmjuiklopoiuytrewsdcfdtyhgfdewasfgtyuikjhgrtrertyuiopiuhgfdsertyuijhgfdsertyujnmbvf"; 
    let word = "tgbggfug"
    let temp=[]; 
    for(let i = 0; i < 100; i++){
        temp = string.split(''); 
        matrix.push(temp); 
    }
    //console.log(matrix)
    return wordSearch(matrix, word, 100, 100); 
}
//console.log(matrix)
assert(testSmallFound());  
assert(testLargeFound());
assert(!testSmallNotFound()); 
assert(!testLargeNotFound()); 

//for(let w = 0; w<words.length; w++){
//    assert(!wordSearch(matrix, words[w], rows, col),"true"); 
//}