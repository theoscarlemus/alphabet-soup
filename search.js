// Need to search for a word in a 2D Array of characters. 
//There are 8 directions to search if the first character matches. (-1,0),(-1,1),(-1,-1),(0,-1),(0,1),(1,1),(1,-1),(1,0)


//Search for a word, the row/col is the starting point
function search(matrix, row, col, word, C, R){
    //Create return object to track locations
    let result = {
        found: false, 
        word: word, 
        startX: -9999999, 
        startY: -9999999, 
        endX:   -9999999,
        endY:   -9999999
    }; 
    //if the starting point doesn't match the first character return false
    if(matrix[row][col] != word[0])
    {
        return result; 
    }
    //found the first letter 
    result.startX = row; 
    result.startY = col; 
    //x-axis of 8 directions 
    let x = [-1,-1,-1,0,0,1,1,1];
    //y-axis of 8 directions 
    let y = [0,1,-1,-1,1,1,-1,0];

    //Search the 8 directions
    for(let dir = 0; dir < 8; dir++){
        let i, sx = row + x[dir], sy= col + y[dir]; 
        //stay in the grid
        for(i = 1; i < word.length; i++){
            if(sx >= R || sx < 0 || sy >= C || sy < 0)
            {
                break;
            }
            if(matrix[sx][sy] != word[i])
            {
                break; 
            }
            //save the end of the letter
            result.endX = sx; 
            result.endY = sy; 
            //move to the next letter
            sx += x[dir];
            sy += y[dir];
        } 
        //found the whole word
        if(i == word.length)
        {
            result.found = true; 
            return result; 
        }
    } 
    //catch all 
    return result; 
}

function wordSearch(matrix, word, C, R){
    //Every element is a possible starting point 
    for(let row = 0; row < R; row++){
        for(let col = 0; col < C; col++){
            result = search(matrix, row, col, word, C, R); 
            if(result.found)
            {
                console.log(`${word} ${result.startX}:${result.startY} ${result.endX}:${result.endY}`);
                return true; 
            }
        }
    }
    return false; 
}

module.exports =  {wordSearch};