Program to find if a word exists in a grid. 
Written by Oscar Lemus as a challenge for EITC programing challenge 

This program accepts a file as input in the following format. 

3x3
A B C
D E F
G H I
ABC
AEI

The first line is the dimensions of the grid. 
The next group is the grid. 
The next group are words to look for in the grid. 

run the program the following way. 

node ./main.js <filepath> 